import datetime
from django import template
from crowdvance.recentstories.models import FundraiserStory, SuccessStory

register = template.Library()


@register.assignment_tag
def fundraiser_stories_tag():
    return FundraiserStory.objects.order_by("?")[:3]


@register.assignment_tag
def success_stories_tag():
    return SuccessStory.objects.order_by("?")[:3]