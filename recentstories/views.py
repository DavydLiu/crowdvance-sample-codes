from crowdvance.recentstories.models import FundraiserStory, SuccessStory
from django.views.generic import DetailView, ListView

class FundraiserStoryView(DetailView):
    
    model = FundraiserStory
    
    
class FundraiserStoriesView(ListView):
    
    queryset = FundraiserStory.objects.order_by('-time')
    
    
class SuccessStoryView(DetailView):
    
    model = SuccessStory
    
    
class SuccessStoriesView(ListView):
    
    model = SuccessStory
    