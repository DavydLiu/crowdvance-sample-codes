from django.db import models

from sorl.thumbnail import ImageField


class FundraiserStory(models.Model):
    title = models.CharField(max_length=255)
    sub_title = models.CharField(max_length=255)
    description = models.TextField()
    photo = ImageField(upload_to="story_photos")
    answer_1 = models.TextField(blank=True)
    answer_2 = models.TextField(blank=True)
    answer_3 = models.TextField(blank=True)
    answer_4 = models.TextField(blank=True)
    answer_5 = models.TextField(blank=True)
    slug = models.ForeignKey('fundraisers.fundraiser', to_field='slug')
    time = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name_plural = "fundraiser stories"

    
class SuccessStory(models.Model):
    title = models.CharField(max_length=255)
    sub_title = models.CharField(max_length=255)
    description = models.TextField()
    photo = ImageField(upload_to="story_photos")
    answer_1 = models.TextField(blank=True)
    answer_2 = models.TextField(blank=True)
    answer_3 = models.TextField(blank=True)
    answer_4 = models.TextField(blank=True)
    answer_5 = models.TextField(blank=True)
    answer_6 = models.TextField(blank=True)
    slug = models.ForeignKey('fundraisers.fundraiser', to_field='slug')
    time = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name_plural = "success stories"