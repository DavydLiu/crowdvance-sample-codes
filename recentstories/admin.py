from django.contrib import admin
from crowdvance.recentstories.models import FundraiserStory, SuccessStory

class FundraiserStoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'time')

class SuccessStoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'time')

admin.site.register(FundraiserStory, FundraiserStoryAdmin)
admin.site.register(SuccessStory, SuccessStoryAdmin)