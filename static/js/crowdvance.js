function ajaxLoader (el, options) {
  // Becomes this.options
  var defaults = {
    bgColor     : '#fff',
    duration    : 800,
    opacity     : 0.7,
    classOveride  : false
  };
  this.options  = jQuery.extend(defaults, options);
  this.container  = $(el);
  this.init = function() {
    var container = this.container;
    // Delete any other loaders
    this.remove();
    // Create the overlay
    var overlay = $('<div></div>').css({
        'background-color': this.options.bgColor,
        'opacity':this.options.opacity,
        'width':container.innerWidth(),
        'height':container.innerHeight(),
        'position':'absolute',
        'top':'0px',
        'left':'0px',
        'z-index':99999
    }).addClass('ajax_overlay');
    // add an overiding class name to set new loader style
    if (this.options.classOveride) {
      overlay.addClass(this.options.classOveride);
    }
    // insert overlay and loader into DOM
    container.append(
      overlay.append(
        $('<div></div>').addClass('ajax_loader')
      ).fadeIn(this.options.duration)
    );
    };
  this.remove = function(){
    var overlay = this.container.children(".ajax_overlay");
    if (overlay.length) {
      overlay.fadeOut(this.options.classOveride, function() {
        overlay.remove();
      });
    }
  };
    this.init();
}

(function($) {

    $.fn.typer = function(options) {
        var defaults = {speed: 50},
            settings = $.extend({}, defaults, options);
        return this.each(function(e,options){
            var el = $(this),
                text = el.val(),
                chars = 0,
                timeout = null,
                tw = function() {
                    el.val(text.substr(0, chars));
                    chars += 1;
                    timeout = setTimeout(function(){tw();}, settings.speed);
                    if(text.length === chars){clearTimeout(timeout);}
                };
            
            el.html("");
            tw();
            while (timeout == null);
        });
    };

})(jQuery);

$(document).ready(function(){

    $('.top-bar').fixTo('body');

    $("#introVideo").on('show', function() {
        $("#introVideo .video").html('<iframe frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen width="640" height="360" name="vidly-frame" id="vidly-frame" src="http://s.vid.ly/embeded.html?link=1o1w4n&new=1&autoplay=true"><a target="_blank" href="http://vid.ly/1o1w4n"><img src="http://vid.ly/1o1w4n/poster" /></a></iframe>');
    });
    $("#introVideo").on('hide', function() {
        $("#introVideo .video").html('');
    });

    $(".submit").click(function(e) {
      $(this).closest("form").submit();
      e.preventDefault();
      return false;
    });

    $('.dropdown-toggle').dropdown();
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
      });
    $('.fileupload').fileupload('image');
    $.each($("input[type!=checkbox], textarea, select"), function() {
        if ($('p[name="'+ $(this).attr('id') +'"]').html())
        {
            $(this).popover({"trigger": "focus", "content": $('p[name="'+ $(this).attr('id') +'"]').html(), "title": $('label[for="'+ $(this).attr('id') +'"]').text()});
        } 
    });
    $('#id_slug').slugify('#id_fundraiser_name'); 

if(!Modernizr.input.placeholder){

  $('[placeholder]').focus(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
    input.val('');
    input.removeClass('placeholder');
    }
  }).blur(function() {
    var input = $(this);
    if (input.val() == '' || input.val() == input.attr('placeholder')) {
    input.addClass('placeholder');
    input.val(input.attr('placeholder'));
    }
  }).blur();
  $('[placeholder]').parents('form').submit(function() {
    $(this).find('[placeholder]').each(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
      input.val('');
    }
    });
  });

  }
});
