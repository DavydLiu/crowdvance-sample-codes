$(document).ready(function() {

    $(".editBox").click(function() {
        $("#" + $(this).attr("name")).modal();
    });
    $(".save-form").click(function() {
        var $form = $("#dashboard_update_form");
        var modal = $(this).parents(".modal");
        clear_form_field_errors();

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            error: function(xhr, status, error) {
                //Actions to take on error
            },
            success: closeForm(modal)
        });
         return false;
    });

    var closeForm = function(modal) {
        return function(response) {
            if (response.success) {
                modal.modal('hide');
            } else {
                $.each(response.data, function(index, value) {
                    if (index === "__all__") {
                        django_message(value[0], "error");
                    } else {
                        apply_form_field_error(index, value);
                    }
                });
            }
        };
    };
    var preEditText = {};
    $('.modal').on('show', function () {
      preEditText[$(this).attr("id")] = $(this).find("textarea").val();
    });
    $(".cancel").click(function() {
        var modal = $(this).parents(".modal");
        modal.find("textarea").val(preEditText[$(modal).attr("id")]);
    });
    $(".cancel").click(function() {
        var modal = $(this).parents(".modal");
        modal.find("textarea").val(preEditText[$(modal).attr("id")]);
    });
    $("#fundraiser-url").click(function() { $(this).select(); } );
});

    

function apply_form_field_error(fieldname, error) {
    var input = $("#id_" + fieldname),
        container = $("#div_id_" + fieldname),
        error_msg = $("<span />").addClass("help-inline ajax-error").text(error[0]);

    container.addClass("error");
    error_msg.insertAfter(input);
}

function clear_form_field_errors() {
    $(".ajax-error").remove();
    $(".error").removeClass("error");
}

function django_message(msg, level) {
    var levels = {
        warning: 'alert',
        error: 'error',
        success: 'success',
        info: 'info'
    },
    source = $('#message_template').html(),
    template = Handlebars.compile(source),
    context = {
        'tags': levels[level],
        'message': msg
    },
    html = template(context);

    $("#message_area").append(html);
}

function showOverlay(el)
{
    var options = {bgColor: '#000', opacity: 0.5, duration: 250};
    var loader = new ajaxLoader(el, options);
    return loader;
}
function hideOverlay(overlay)
{
    overlay.remove();
}

function show_django_field_errors(data)
{
    $.each(data, function(index, value) {
        if (index === "__all__") {
            django_message(value[0], "error");
        } else {
            apply_form_field_error(index, value);
        }
    });
}

