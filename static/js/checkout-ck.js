function switch_to_step(e){
    $(".step-"+e+" > .circle-icon").animate({backgroundColor:"#444444",color:"#efede3"},"slow");
    $(".step-"+e+" > .icon").animate({backgroundColor:"#444444",color:"#efede3"},"slow");
    $(".progress-indicator-circle .step-"+(e-1)).fadeOut(500);
    $(".progress-indicator-circle .step-"+e).fadeIn(500)
}
function toggleFundraiserSidebar(){
    $(".left").toggleClass("full");
    $(".sidebar").toggleClass("hideSidebar showSidebar")
}
WePay.set_endpoint(wepay_endpoint);
$("document").ready(function(){
    // Hide sidebar in step 1
    $(".partner-sidebar").hide();
    $(".focus").first().focus();
    $(".crowdvance-form input").keydown(function(e){e.keyCode==13&&jQuery.support.submitBubbles&&$(e.target).parent().find(".submit").trigger("click")});
    $(".donation-form").on("submit",function(){var e=$(this);
        clear_form_field_errors();
        var t=showOverlay(e.parent());
        $.ajax({type:"POST",data:e.serialize(),error:function(e,t,n){alert("Error submitting form. Please try again in a few moments.")},success:function(n){
            if(n.success){
                e.parent().hide();
                // Display partner selection pop-ups
                $(".partner-selection").parent().show();
                $("input[name='donation_id']").val(n.donation_id);
                $(".donation-amount").text(n.donation_amount);
                switch_to_step(2);
                hideOverlay(t);
                toggleFundraiserSidebar();
                $(".partner-sidebar").html("")
            } else {
                hideOverlay(t);
                show_django_field_errors(n.data)
            }
        }
        });
        return!1
    });
    $(".partner-selection").on("submit",function(){
        var e=$(this);
        clear_form_field_errors();
        var t=showOverlay(e);
        $.ajax({type:"POST",data:e.serialize(),url:e.attr("action"),error:function(e,n,r){t.remove();
            alert(r)},success:function(n){
            if(n.success){
                $(".fundraiser-info").hide();
                e.parent().hide();
                $(".cc-form").parent().show();
                switch_to_step(3);
                hideOverlay(t);
                toggleFundraiserSidebar();
                $(".partner-sidebar").show();
                $(".cc-form").find(".focus").focus()
            } else {
                hideOverlay(t);
                show_django_field_errors(n.data)
            }}
        });
        return!1
    });
    $(".cc-form").on("submit",function(){
        var e=$(this),t=showOverlay(
            e.parent()),
            n=WePay.credit_card.create({
                client_id:wepay_client_id,user_name:$("#billing_name").val(),email:$("#id_email").val(),cc_number:$("#cc_number").val(),cvv:$("#cvv_code").val(),expiration_month:$("#exp_month").val(),expiration_year:$("#exp_year").val(),address:{address1:$("#billing_address").val(),city:$("#city").val(),state:$("#state option:selected").val(),country:"US",zip:$("#zip").val()}
            },
            function(n) {
                if (n.error) {
                    $("#error_placeholder").html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><span><strong>'+n.error_description+"</strong></span></div>");
                    hideOverlay(t)
                } else {
                    $("#cc_token").val(n.credit_card_id);
                    $.ajax({
                        type:"POST",url:e.attr("action"),data:e.serialize(),error:function(e,n,r){
                            hideOverlay(t);
                            $("#error_placeholder").html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><span><strong>'+r+"</strong></span></div>")
                        },success:function(n){
                            $("#error_placeholder").hide();
                            e.parent().hide();
                            // Display gifts
                            $("#rewards").html(n.rewards);
                            $("#rewards").show();
                            $(".partner-sidebar").hide();
                            $(".share-donation").fadeIn();
                            addthis.toolbox(".addthis_toolbox");
                            switch_to_step(4);
                            hideOverlay(t)
                        }
                    })
                }
            return!1
        });
        if(n.error){
            $("#error_placeholder").html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><span><strong>'+n.error_description+"</strong></span></div>");
            hideOverlay(t)
        }
        return!1
    });
    var e=0;
    // Toggle partner selection
    $("form .partner").on(
        "click",function(){
            var t=$("input[value="+$(this).attr("name")+"]");
            this_partner_everywhere=$(".partner[name='"+$(this).attr("name")+"']");
            if (t.prop("checked")) {
                e--;
                $("input[value="+$(this).attr("name")+"]").prop("checked",!1);
                this_partner_everywhere.removeClass("selected");
                $(".partner-placeholder > img[name="+$(this).attr("name")+"]").parent().remove();
                $(".thumbnail-holder").append("<div class='partner-placeholder empty'></div>");
                $(".partner-sidebar>div[name='"+$(this).attr("name")+"']").remove()
            } else {
                // Check if the selection reaches maximum number
                if(e>=max_rewards)return;
                e++;
                $("input[value="+$(this).attr("name")+"]").prop("checked",!0);
                this_partner_everywhere.addClass("selected");
                // Add mini-thumbnail to the first empty div
//                $(".empty:first").removeClass("empty").html($(this).find(".mini-thumbnail").html());
                // Append selected partner to sidebar
                $(".partner-sidebar").append($(this).children(".partner-sidebar-html").children('.partner').removeClass('selected'))
            }
        }
    )
});
