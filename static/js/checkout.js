WePay.set_endpoint(wepay_endpoint); // change to "production" when live
$("document").ready(function() {
    $(".focus").first().focus();
    $(".crowdvance-form input").keydown(function(event){
        if(event.keyCode == 13 && jQuery.support.submitBubbles) {
            $(event.target).parent().find(".submit").trigger("click");
        }
    });
    $(".donation-form").on("submit", function() {
        var $form = $(this);
        clear_form_field_errors();
        var loader = showOverlay($form.parent());
        $.ajax({
            type: "POST",
            data: $form.serialize(),
            error: function(xhr, status, error) {
                alert("Error submitting form. Please try again in a few moments.");
            },
            success: function(response) {
                if (response.success) {
                    $form.parent().hide();
                    $(".partner-selection").parent().show();
                    $("input[name='donation_id']").val(response.donation_id);
                    $(".donation-amount").text(response.donation_amount);
                    switch_to_step(2);
                    hideOverlay(loader);
                    toggleFundraiserSidebar();
                    $(".partner-sidebar").html("");
                } else {
                    hideOverlay(loader);
                    show_django_field_errors(response.data);
                }
            }
        });
        return false;
    });
    $(".partner-selection").on("submit", function() {
        var $form = $(this);
        clear_form_field_errors();
        var loader = showOverlay($form);
        $.ajax({
            type: "POST",
            data: $form.serialize(),
            url: $form.attr('action'),
            error: function(xhr, status, error) {
                loader.remove();
                alert(error);
            },
            success: function(response) {
                if (response.success) {
                    $(".fundraiser-info").hide();
                    $form.parent().hide();
                    $(".cc-form").parent().show();
                    switch_to_step(3);
                    hideOverlay(loader);
                    toggleFundraiserSidebar();
                    $(".cc-form").find(".focus").focus();
                } else {
                    hideOverlay(loader);
                    show_django_field_errors(response.data);
                }
            }
        });
        return false;
    });
    $(".cc-form").on("submit", function() {
        var $form = $(this);
        var loader = showOverlay($form.parent());
        var response = WePay.credit_card.create( {
            "client_id":wepay_client_id,
            "user_name":$("#billing_name").val(),
            "email":$("#id_email").val(),
            "cc_number":$("#cc_number").val(),
            "cvv":$("#cvv_code").val(),
            "expiration_month":$("#exp_month").val(),
            "expiration_year":$("#exp_year").val(),
            "address":
            {
                "address1":$("#billing_address").val(),
                "city":$("#city").val(),
                "state":$("#state option:selected").val(),
                "country":"US",
                "zip":$("#zip").val()
            }
        }, function(data) {
            if (data.error) {
                $('#error_placeholder').html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><span><strong>' + data.error_description + '</strong></span></div>');
                hideOverlay(loader);
            } else {
                $("#cc_token").val(data.credit_card_id);
                $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    error: function(xhr, status, error) {
                        hideOverlay(loader);
                        $('#error_placeholder').html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><span><strong>' + error + '</strong></span></div>');
                    },
                    success: function(data) {
                        $('#error_placeholder').hide();
                        $form.parent().hide();
                        $("#rewards").html(data.rewards);
                        $("#rewards").show();
                        $(".partner-sidebar").hide();
                        $(".share-donation").fadeIn();
                        addthis.toolbox(".addthis_toolbox");
                        switch_to_step(4);
                        hideOverlay(loader);
                    }
                });
            }
            return false;
        });
        if (response.error) {
            $('#error_placeholder').html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><span><strong>' + response.error_description + '</strong></span></div>');
            hideOverlay(loader);
        }
        return false;
    });

    var selectedPartners = 0;
    $('form .partner').on("click", function() {
        var checkbox = $("input[value="+$(this).attr("name")+"]");
        this_partner_everywhere = $(".partner[name='" + $(this).attr("name") + "']");
        if (checkbox.prop("checked"))
        {
            selectedPartners--;
            $("input[value="+$(this).attr("name")+"]").prop("checked", false);
            this_partner_everywhere.removeClass("selected");
            $(".partner-placeholder > img[name="+$(this).attr("name")+"]").parent().remove();
            $(".thumbnail-holder").append("<div class='partner-placeholder empty'></div>");
            $(".partner-sidebar>div[name='" + $(this).attr("name") + "']").remove();

        } else {
            if (selectedPartners >= max_rewards)
                return;
            selectedPartners++;
            $("input[value="+$(this).attr("name")+"]").prop("checked", true);
            this_partner_everywhere.addClass("selected");
            $(".empty:first").removeClass("empty").html($(this).find(".mini-thumbnail").html());
            $(".partner-sidebar").append($(this).clone().removeClass("selected"));
        }
    });
});

function switch_to_step(step_num)
{
    $(".step-" + step_num + " > .circle-icon").animate({backgroundColor: '#444444', color: '#efede3'}, 'slow');
    $(".step-" + step_num + " > .icon").animate({backgroundColor: '#444444', color: '#efede3'}, 'slow');
    $(".progress-indicator-circle .step-" + (step_num-1)).fadeOut(500);
    $(".progress-indicator-circle .step-" + step_num).fadeIn(500);
}

function toggleFundraiserSidebar()
{
    $('.left').toggleClass('full');
    $('.sidebar').toggleClass('hideSidebar showSidebar');
}