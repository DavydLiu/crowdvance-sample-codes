from wepay import WePay
from django.conf import settings
from decimal import Decimal
from django.core.urlresolvers import reverse_lazy
from django.contrib.sites.models import Site
import urllib


class WePayIntegration():

    @staticmethod
    def create_donation_iframe(account_id, access_token, amount, fundraiser):
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION, access_token)
        app_fee = amount * Decimal(settings.CROWDVANCE_COMMISSION)
        redirect_uri = 'http://%s%s' % (Site.objects.get_current().domain,
                                        reverse_lazy("fundraiser_rewards",
                                                     kwargs={"slug": fundraiser.slug}))
        callback_uri = ""
        if "localhost" not in Site.objects.get_current().domain:
            callback_uri = 'http://%s%s' % (Site.objects.get_current().domain,
                                        reverse_lazy("wepay_ipn",
                                                     kwargs={"slug": fundraiser.slug}))
        response = we_pay.call('/checkout/create', {
            'account_id': account_id,
            'amount': "%.2f" % amount,
            'short_description': settings.WEPAY_DONATION_DESCRIPTION % fundraiser.fundraiser_name,
            'type': 'DONATION',
            'app_fee': "%.2f" % app_fee,
            'fee_payer': 'Payee',
            'mode': 'iframe',
            'redirect_uri': redirect_uri,
            'callback_uri': callback_uri,
        })
        return response

    @staticmethod
    def charge_credit_card(account_id, access_token, donation, cc_token):
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION, access_token)
        app_fee = donation.amount * Decimal(settings.CROWDVANCE_COMMISSION)
        callback_uri = ""
        if "localhost" not in Site.objects.get_current().domain:
            callback_uri = 'http://%s%s' % (Site.objects.get_current().domain,
                                        reverse_lazy("wepay_ipn",
                                                     kwargs={"slug": donation.fundraiser.slug}))
        response = we_pay.call('/checkout/create', {
            'account_id': account_id,
            'amount': "%.2f" % donation.amount,
            'short_description': settings.WEPAY_DONATION_DESCRIPTION % donation.fundraiser.fundraiser_name,
            'type': 'DONATION',
            'payment_method_id': cc_token,
            'payment_method_type': 'credit_card',
            'app_fee': "%.2f" % app_fee,
            'fee_payer': 'Payee',
            'callback_uri': callback_uri,
        })
        return response

    @staticmethod
    def get_checkout_status(wepay_checkout_id, access_token):
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION, access_token)
        response = we_pay.call('/checkout/', {
            'checkout_id': wepay_checkout_id
        })
        return response

    @staticmethod
    def get_oath2_redirect_url(organization):
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION)
        redirect_uri = 'http://%s%s' % (Site.objects.get_current().domain, reverse_lazy("step3"))
        url_params = urllib.urlencode({
            'client_id': settings.WEPAY_CLIENT_ID,
            'redirect_uri': redirect_uri,
            'scope': settings.WEPAY_SCOPE,
            'user_name': organization.owner.username,
            'user_email': organization.owner.email
        })
        return "%s/oauth2/authorize?%s" % (we_pay.browser_endpoint, url_params)

    @staticmethod
    def get_oath2_token(code):
        redirect_uri = 'http://%s%s' % (Site.objects.get_current().domain, reverse_lazy("step3"))
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION)
        response = we_pay.call('/oauth2/token', {
            'client_id': settings.WEPAY_CLIENT_ID,
            'redirect_uri': redirect_uri,
            'client_secret': settings.WEPAY_CLIENT_SECRET,
            'code': code
        })
        return response

    @staticmethod
    def setup_account(fundraiser):
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION, fundraiser.organization.wepay_token)
        response = we_pay.call('/account/create', {
            'name': fundraiser.fundraiser_name,
            'description': settings.WEPAY_ACCOUNT_DESCRIPTION,
            'reference_id': fundraiser.pk
        })
        return response

    @staticmethod
    def get_account_balance(fundraiser):
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION, fundraiser.organization.wepay_token)
        response = we_pay.call('/account/balance', {
            'account_id': fundraiser.wepay_account,
        })
        return response

    @staticmethod
    def withdraw_funds(fundraiser):
        redirect_uri = 'http://%s%s' % (Site.objects.get_current().domain, reverse_lazy("my_organization_dashboard"))
        we_pay = WePay(settings.WEPAY_USE_PRODUCTION, fundraiser.organization.wepay_token)
        print fundraiser.wepay_account_balance
        response = we_pay.call('/withdrawal/create/', {
            'account_id': fundraiser.wepay_account,
            'amount': fundraiser.wepay_account_balance['available_balance'],
            'redirect_uri': redirect_uri,
            'note': settings.WEPAY_WITHDRAW_NOTE
        })
        return response['withdrawal_uri']
