from django.contrib import admin
from crowdvance.fundraisers.models import Fundraiser, FundraiserImage, Donation
from crowdvance.partners.models import Partner, DiscountCode
from django.contrib.admin import SimpleListFilter
from django.utils.safestring import mark_safe
from daterange_filter.filter import DateRangeFilter

class FundraiserImageInline(admin.StackedInline):
    model = FundraiserImage
    extra = 3


class FundraiserAdmin(admin.ModelAdmin):
    list_display = ('fundraiser_name', 'organization', 'contact_email', 'fundraiser_end_date', 'goal', 'net_amount_raised')
    inlines = [
        FundraiserImageInline,
    ]


class DonationPartnerChosenListFilter(SimpleListFilter):
    title = 'Rewards chosen'
    parameter_name = 'partner'

    def lookups(self, request, model_admin):
        partners = []
        for partner in Partner.objects.all():
            partners.append((partner.slug, partner.partner_name))
        return partners

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(selected_rewards__slug__contains=self.value())
        return queryset


class DonationAdmin(admin.ModelAdmin):
    list_display = ('fundraiser', 'amount', 'donation_time', 'wepay_state', 'codes')
    list_filter = (DonationPartnerChosenListFilter, ('donation_time', DateRangeFilter), 'fundraiser', 'wepay_state')

    def codes(self, donation):
        partners = Partner.objects.all()
        # partners = donation.selected_rewards.all()
        codes = []
        for partner in partners:
            if partner in donation.selected_rewards.all():
                try:
                    codes.append(DiscountCode.objects.filter(partner=partner,
                                                                 donation=donation)[0].code)
                except:
                    codes.append(DiscountCode.objects.filter(partner=partner,
                                                             donation=None,
                                                             active=True)[0].code)
            else:
                codes.append("X")
        return '</td><td>'.join(codes)

    def partner_titles():
        partner_headers = []
        for partner in Partner.objects.all():
            partner_headers.append(partner.partner_name)
        return '</th><th>'.join(partner_headers) + '<th>'

    codes.allow_tags = True
    codes.short_description = mark_safe(partner_titles())

admin.site.register(Fundraiser, FundraiserAdmin)
admin.site.register(Donation, DonationAdmin)