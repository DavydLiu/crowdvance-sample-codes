import datetime
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _


from crowdvance.fundraisers.models import Fundraiser, Donation
from crowdvance.partners.models import Partner


class DollarSignInput(forms.TextInput):
        pass


class PhotoInput(forms.FileInput):
    help_text=_("This will appear as the title on your fundraising page.")
    pass


class DonationForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField()
    amount = forms.DecimalField(decimal_places=2)
    is_anonymous = forms.BooleanField(required=False)
    donation_note = forms.CharField(widget=forms.widgets.Textarea(), required=False)

    def clean_amount(self):
        amount = self.cleaned_data['amount']
        if amount < settings.MINIMUM_DONATION:
            raise forms.ValidationError(_('Donations must be at least $%s' % settings.MINIMUM_DONATION))
        return amount


class CreditCardForm(forms.Form):
    donation_id = forms.CharField(max_length=255)
    cc_token = forms.CharField(max_length=255)
    billing_city = forms.CharField(max_length=255)
    billing_state = forms.CharField(max_length=255)
    billing_zip = forms.CharField(max_length=255)


class FundraiserCreateForm(forms.ModelForm):
    class DollarSignInput(forms.TextInput):
        pass

    class Meta:
        model = Fundraiser
        fields = ("fundraiser_name", "slug", "blurb", "description", "photo", "goal",
                  "fundraiser_end_date", "thank_you_message")
        widgets = {'photo': PhotoInput,
                   'goal': DollarSignInput(attrs={'placeholder': '0.00',
                                                  'class': 'dollar-sign-addon'}),
                   'fundraiser_end_date': forms.widgets.DateInput(format="%m/%d/%Y")}
    slug = forms.SlugField(label='Fundraiser URL', required=False,
        help_text=_("Your fundraiser's website will be at:<br />http://www.crowdvance.com/fundraisers/<strong><span class='slug-url'>your-url-here</span></strong>"))
    fundraiser_name = forms.CharField(label='Fundraiser Name',
        help_text=_("This will appear as the title on your fundraising page."))
    blurb = forms.CharField(label=('Blurb'),
        required=False,
        help_text=_('This should be a quick call to action that gives people all of the information they need to know about your fundraiser in 140 characters or less! '),
        max_length=140)
    description = forms.CharField(widget=forms.widgets.Textarea(),
        label=_('Description'),
        help_text=_("This is your \"ask.\" What are you fundraising for? What's your organization, team, or cause about? Make it count!"))
    fundraiser_end_date = forms.DateField(widget=forms.widgets.DateInput(format="%m/%d/%Y"),
        label=_('End Date'),
        help_text=_("Your fundraiser can last for a maximum of six weeks"),
        initial=(datetime.date.today() + datetime.timedelta(settings.MAXIMUM_FUNDRAISER_LENGTH)))
    thank_you_message = forms.CharField(widget=forms.widgets.Textarea(),
        label=_('Thank you message'),
        help_text=_("This message will be shown to your donors upon successful donation. You'll be able to edit this, along with everything else, from your dashboard."))


    def clean_fundraiser_end_date(self):
        date = self.cleaned_data['fundraiser_end_date']
        if date > datetime.date.today() + datetime.timedelta(settings.MAXIMUM_FUNDRAISER_LENGTH):
            raise forms.ValidationError(_('Fundraisers cannot run for longer than %s days' % settings.MAXIMUM_FUNDRAISER_LENGTH))
        return date


class FundraiserUpdateForm(forms.ModelForm):
    class Meta:
            model = Fundraiser
            fields = ("blurb", "thank_you_message", "description", "photo")

    blurb = forms.CharField(required=False)
    thank_you_message = forms.CharField(required=False, widget=forms.widgets.Textarea())
    description = forms.CharField(required=False, widget=forms.widgets.Textarea())
    photo = forms.ImageField(required=False, widget=PhotoInput())


class FundraiserSelectRewardsForm(forms.ModelForm):
    partners = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        required=False,
        queryset=Partner.objects
    )

    class Meta:
            model = Fundraiser
            fields = ("partners",)

    def clean(self):
        cleaned_data = super(FundraiserSelectRewardsForm, self).clean()
        if len(cleaned_data['partners']) == 0:
            raise forms.ValidationError("You must choose at least one reward!")
        return cleaned_data


class FundraiserChooseRewardsForm(forms.ModelForm):
    selected_rewards = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        required=False,
        queryset=Partner.objects
    )
    donation_id = forms.CharField(max_length=255)

    class Meta:
            model = Donation
            fields = ("selected_rewards",)

    def clean(self):
        cleaned_data = super(FundraiserChooseRewardsForm, self).clean()
        if len(cleaned_data['selected_rewards']) == 0:
            raise forms.ValidationError("You must choose at least one reward!")
        elif len(cleaned_data['selected_rewards']) > settings.NUMBER_OF_REWARDS_PER_DONATION:
            raise forms.ValidationError("You chose too many rewards. Don't do that!")
        return cleaned_data
