import datetime
from django import template
from crowdvance.fundraisers.models import Fundraiser

register = template.Library()


@register.assignment_tag
def recent_fundraisers():
    return Fundraiser.objects.filter(is_active=True, fundraiser_end_date__gt=datetime.datetime.today()).order_by("?")[:16]
