import json
from datetime import datetime, date, timedelta
from decimal import Decimal
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import View, RedirectView
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseForbidden
from django import forms
from django.template.loader import render_to_string
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.contrib.localflavor.us.us_states import US_STATES
from django.utils import timezone

from django.shortcuts import render_to_response
from django.template import RequestContext

from crowdvance.fundraisers.models import Fundraiser, Donation
from crowdvance.fundraisers.forms import DonationForm, CreditCardForm
from django.views.generic.edit import FormView
from crowdvance.fundraisers.wepay_helper import WePayIntegration
from crowdvance.fundraisers.forms import (FundraiserCreateForm,
                                          FundraiserUpdateForm,
                                          FundraiserChooseRewardsForm)
from crowdvance.partners.models import Partner, PartnerCategory

def take_a_spin(request):
    
    context = {'fundraiser': {'slug': 'take-a-spin', 
                              'fundraiser_name': request.POST['name'], 
                              'fundraiser_donate': 'take-a-spin', 
                              'description': "This is where you will enter your organization, team, or cause's message that all of your donors will see. Why are you fundraising? What is your organization or team about? Everyone that comes to your page will see this information, so make sure to think about what you want here! You can edit this text from your Account Dashboard when you log-in to your account after registration.", 
                              'photo': 'fundraiser_photos/take-a-spin-wide.jpg',
                              'launch_date': datetime.now(),
                              'fundraiser_end_date': date.today() + timedelta(settings.MAXIMUM_FUNDRAISER_LENGTH),
                              'organization': {'city': request.POST['city'],
                                               'get_state_display': request.POST['state'],
                                               'facebook': 'facebook.com/crowdvance',
                                               'twitter': 'twitter.com/crowdvance',
                                               'website': 'crowdvance.com',
                                               'owner': {'username': 'myorganization',
                                                         'email': 'info@crowdvance.com'}}
                              }}
#    import pdb; pdb.set_trace()
    if context['fundraiser']['fundraiser_name'] == '':
        context['fundraiser']['fundraiser_name'] = 'Take a Spin'
    if context['fundraiser']['organization']['city'] == '':
        context['fundraiser']['organization']['city'] = 'My City'
    if context['fundraiser']['organization']['get_state_display'] == '':
        context['fundraiser']['organization']['get_state_display'] = 'My State'

    return render_to_response('fundraisers/fundraiser_detail.html', context, context_instance=RequestContext(request, context))


class FundraiserEditView(UpdateView):

    model = Fundraiser
    form_class = FundraiserCreateForm

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(FundraiserEditView, self).get_context_data(**kwargs)
        context['edit'] = True
        return context

    def get_success_url(self):
        return reverse_lazy("fundraiser_detail", kwargs={"slug": self.object.slug})


class FundraiserCreateView(CreateView):

    model = Fundraiser
    form_class = FundraiserCreateForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(FundraiserCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if not self.request.user.organization.completed_wepay:
            return self.form_invalid(form)
        form.instance.organization = self.request.user.organization
        form_valid = super(FundraiserCreateView, self).form_valid(form)
        wepay_account = WePayIntegration.setup_account(self.object)
        self.object.setup_wepay_account(wepay_account['account_id'])
        self.object.start_fundraiser(self.request)
        return form_valid

    def get_success_url(self):
        return reverse_lazy("fundraiser_detail", kwargs={"slug": self.object.slug})


class FundraiserDetailView(DetailView):

    model = Fundraiser
    
    def get_context_data(self, **kwargs):
        context = super(FundraiserDetailView, self).get_context_data(**kwargs)
        context['show_donate_button'] = True
        if self.get_object().organization.owner == self.request.user:
            context['fundraiser_owner'] = True
            context['new_fundraiser'] = self.object.show_getting_started
        return context
    

class NewFundraiserDetailView(FundraiserDetailView):
    pass


class FundraiserDashboardView(UpdateView):

    model = Fundraiser
    form_class = FundraiserUpdateForm
    template_name = "fundraisers/fundraiser_dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(FundraiserDashboardView, self).get_context_data(**kwargs)
        context['show_donate_button'] = False
        context['hide_rewards'] = True
        context['wepay_balance'] = self.object.wepay_account_balance
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        dispatch = super(FundraiserDashboardView, self).dispatch(request, *args, **kwargs)
        if self.get_object().organization.owner != request.user and not request.user.is_superuser:
            return HttpResponseForbidden()
        return dispatch

    def form_valid(self, form):
        self.object = form.save()
        print form.cleaned_data['photo']
        if form.cleaned_data['photo'] is self.object.photo:
            json_data = json.dumps({"success": True})
            return HttpResponse(json_data, mimetype="application/json")
        else:
            return super(FundraiserDashboardView, self).form_valid(form)

    def form_invalid(self, form):
        if form.cleaned_data['photo'] is self.object.photo:
            json_data = json.dumps({"success": False})
            return HttpResponse(json_data, mimetype="application/json")
        else:
            return super(FundraiserDashboardView, self).form_invalid(form)

    def get_success_url(self):
        return reverse_lazy("fundraiser_dashboard", kwargs={"slug": self.object.slug})


class FundraiserDonateView(FormView, SingleObjectMixin):

    form_class = DonationForm
    template_name = "fundraisers/fundraiser_donate.html"
    model = Fundraiser

    def dispatch(self, request, *args, **kwargs):
        dispatch = super(FundraiserDonateView, self).dispatch(request, *args, **kwargs)
        if self.get_object().expired:
            return redirect(reverse_lazy("fundraiser_detail",
                                         kwargs={"slug": self.get_object().slug}))
        return dispatch

    def form_valid(self, form):
        donation = Donation(**form.cleaned_data)
        donation.amount = form.cleaned_data['amount']
        donation.fundraiser = self.get_object()
        donation.wepay_state = Donation.WEPAY_NEW
        donation.save()
        json_data = json.dumps({"success": True, "donation_id": donation.id,
            "donation_amount": str(donation.amount.quantize(Decimal('1.00')))})
        return HttpResponse(json_data, mimetype="application/json")

    def form_invalid(self, form):
        json_data = json.dumps({"success": False, "data": form.errors})
        return HttpResponse(json_data, mimetype="application/json")

    def get_context_data(self, **kwargs):
        context = {
            'fundraiser': self.get_object(),
            'form': self.get_form(self.get_form_class()),
            'states': US_STATES,
            'wepay_client': settings.WEPAY_CLIENT_ID,
            'partners': Partner.objects.all(),
            'partner_categories': PartnerCategory.objects.all(),
            'max_rewards': settings.NUMBER_OF_REWARDS_PER_DONATION,
            'partner_form': FundraiserChooseRewardsForm,
        }
        return super(FundraiserDonateView, self).get_context_data(**context)


class FundraiserChargeCreditCardView(FormView, SingleObjectMixin):

    form_class = CreditCardForm
    model = Fundraiser

    def form_valid(self, form):
        fundraiser = self.get_object()
        org = fundraiser.organization
        donation = Donation.objects.get(pk=form.cleaned_data['donation_id'])
        charge = WePayIntegration.charge_credit_card(fundraiser.wepay_account,
            org.wepay_token, donation, form.cleaned_data['cc_token'])
        donation.wepay_transaction = charge['checkout_id']
        donation.city = form.cleaned_data['billing_city']
        donation.state = form.cleaned_data['billing_state']
        donation.zip_code = form.cleaned_data['billing_zip']
        donation.save()
        donation.update_wepay_state(charge['state'])
        rewards = donation.get_rewards()
        rewards_template = "fundraisers/fundraiser_rewards_block.html"
        rewards_render = render_to_string(rewards_template, {'rewards': rewards, 'fundraiser': fundraiser})
        json_data = json.dumps({"success": True, "rewards": rewards_render})
        return HttpResponse(json_data, mimetype="application/json")

    def form_invalid(self, form):
        json_data = json.dumps({"success": False, "data": form.errors})
        return HttpResponse(json_data, mimetype="application/json")


class FundraiserChooseRewardsView(FormView, SingleObjectMixin):

    form_class = FundraiserChooseRewardsForm
    model = Fundraiser

    def form_valid(self, form):
        donation = Donation.objects.get(pk=form.cleaned_data['donation_id'])
        donation.selected_rewards = form.cleaned_data['selected_rewards']
        donation.save()
        json_data = json.dumps({"success": True})
        return HttpResponse(json_data, mimetype="application/json")

    def form_invalid(self, form):
        json_data = json.dumps({"success": False, "data": form.errors})
        return HttpResponse(json_data, mimetype="application/json")


class FundraiserRewardsView(DetailView):

    model = Fundraiser
    template_name = "fundraisers/fundraiser_rewards.html"

    def get_context_data(self, **kwargs):
        context = super(FundraiserRewardsView, self).get_context_data(**kwargs)
        if 'checkout_id' in self.request.GET:
            checkout_id = self.request.GET['checkout_id']
            try:
                donation = Donation.objects.get(wepay_transaction=checkout_id)
                context['rewards'] = donation.get_rewards()
                context['hide_rewards'] = True
                context['donation'] = donation
                context['rewards_available'] = donation.rewards_available
            except Exception, e:
                raise(e)
        context['show_donate_button'] = False
        return context


class FundraiserWePayIPNView(View, SingleObjectMixin):

    model = Fundraiser

    def post(self, request, *args, **kwargs):
        checkout_id = request.POST['checkout_id']
        access_token = self.get_object().organization.wepay_token
        checkout_status = WePayIntegration.get_checkout_status(checkout_id, access_token)
        donation = get_object_or_404(Donation, wepay_transaction=checkout_id)
        donation.update_wepay_state(checkout_status['state'])
        return HttpResponse(donation.wepay_state)

    def get(self, request, *args, **kwargs):
        checkout_id = request.GET['checkout_id']
        access_token = self.get_object().organization.wepay_token
        checkout_status = WePayIntegration.get_checkout_status(checkout_id, access_token)
        donation = get_object_or_404(Donation, wepay_transaction=checkout_id)
        donation.update_wepay_state(checkout_status['state'])
        return HttpResponse(donation.wepay_state)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FundraiserWePayIPNView, self).dispatch(*args, **kwargs)


class WepayWithdrawalRedirectView(RedirectView, SingleObjectMixin):

    model = Fundraiser

    def get_redirect_url(self, **kwargs):
        return WePayIntegration.withdraw_funds(self.get_object())

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(WepayWithdrawalRedirectView, self).dispatch(*args, **kwargs)
