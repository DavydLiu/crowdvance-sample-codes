from __future__ import division
import datetime
from django.utils import timezone
from django.db import models
from django.contrib import messages
from django.conf import settings
from django.db.models import Sum
from django.core.mail import EmailMessage

from django.template.loader import render_to_string
from decimal import Decimal
from django.utils.translation import ugettext as _

from sorl.thumbnail import ImageField

from crowdvance.organizations.models import Organization
from crowdvance.fundraisers.wepay_helper import WePayIntegration


class Fundraiser(models.Model):
    fundraiser_name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, unique=True)
    organization = models.ForeignKey(Organization, related_name='fundraisers')
    blurb = models.CharField(max_length=140, blank=True)
    description = models.TextField()
    photo = ImageField(upload_to="fundraiser_photos")
    goal = models.DecimalField(max_digits=10, decimal_places=2)
    partners = models.ManyToManyField("partners.Partner")
    launch_date = models.DateTimeField(auto_now_add=True)
    thank_you_message = models.TextField(blank=True, null=True)
    fundraiser_end_date = models.DateField(default=(datetime.date.today() + datetime.timedelta(settings.MAXIMUM_FUNDRAISER_LENGTH)))
    wepay_account = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=False)
    viewed_getting_started = models.BooleanField(default=False)

    def __unicode__(self):
        return (u"%s" % (self.fundraiser_name.encode('utf-8'))).encode('utf-8')

    def start_fundraiser(self, request):
        if not self.is_active:
            self.is_active = True
            self.save()
            messages.add_message(request, messages.SUCCESS, _("Your fundraiser has been started!"))
            message = render_to_string('fundraisers/setup_email.html', {'fundraiser': self})
            msg = EmailMessage(_('Congratulations, your fundraiser is active!'), message, 'info@crowdvance.com', [self.organization.owner.email])
            msg.content_subtype = "html"  # Main content is now text/html
            msg.send()

    def setup_wepay_account(self, account_id):
        self.wepay_account = account_id
        self.save()

    @property
    def show_getting_started(self):
        if not self.viewed_getting_started:
            self.viewed_getting_started = True
            self.save()
            return True
        else:
            return False

    @property
    def expired(self):
        return datetime.date.today() > self.fundraiser_end_date

    @property
    def days_until_expiration(self):
        if datetime.date.today() > self.fundraiser_end_date:
            return 0
        else:
            return (self.fundraiser_end_date - datetime.date.today()).days

    @property
    def net_amount_raised(self):
        if self.completed_donations.count() > 0:
            return self.donations.filter(wepay_state__in=Donation.WEPAY_SUCCESS_STATES).aggregate(sum=Sum('net_amount'))['sum']
        else:
            return 0
        
    @property
    def amount_raised(self):
        if self.completed_donations.count() > 0:
            return self.donations.filter(wepay_state__in=Donation.WEPAY_SUCCESS_STATES).aggregate(sum=Sum('amount'))['sum']
        else:
            return 0

    @property
    def percent_of_goal(self):
        percent = round(self.net_amount_raised / self.goal * 100)
        if self.net_amount_raised > 0 and percent == 0:
            percent = 1
        return percent
    
    @property
    def percent_of_expiration(self):
        if self.days_until_expiration == 0:
            percent = 100
        else:
#            percent = round(100 - (self.days_until_expiration / (self.fundraiser_end_date - self.launch_date.date()).days) * 100)
#            percent = float(float(self.days_until_expiration) / float(self.fundraiser_end_date - self.launch_date.date().days) * 100.00)
#            percent = 0.1
#            percent = float(float(50)/float(100))
#            percent = 50/100
            percent= round(100 - (self.days_until_expiration / (self.fundraiser_end_date - self.launch_date.date()).days) * 100)
        return percent

    @property
    def available_balance(self):
        return 100.00

    @property
    def donation_count(self):
        return self.donations.count()

    @property
    def completed_donation_count(self):
        return self.completed_donations.count()

    @property
    def wepay_account_balance(self):
        return WePayIntegration.get_account_balance(self)

    @property
    def completed_donations(self):
        return self.donations.filter(wepay_state__in=Donation.WEPAY_SUCCESS_STATES)

    @property
    def pending_donations(self):
        return self.donations.filter(wepay_state__in=Donation.WEPAY_PENDING_STATES)

    @property
    def display_donations(self):
        return self.donations.filter(wepay_state__in=Donation.WEPAY_DISPLAY_STATES)

    @property
    def contact_email(self):
        return self.organization.owner.email


class FundraiserImage(models.Model):
    slug = models.ForeignKey(Fundraiser, to_field='slug')
    photo = ImageField(upload_to="fundraiser_photos")


class Donation(models.Model):

    WEPAY_NEW = 0
    WEPAY_AUTHORIZED = 1
    WEPAY_RESERVED = 2
    WEPAY_CAPTURED = 3
    WEPAY_SETTLED = 4
    WEPAY_CANCELLED = 5
    WEPAY_REFUNDED = 6
    WEPAY_CHARGED_BACK = 7
    WEPAY_FAILED = 8
    WEPAY_EXPIRED = 9

    WEPAY_DISPLAY_STATES = [WEPAY_CAPTURED, WEPAY_SETTLED, WEPAY_AUTHORIZED]
    WEPAY_SUCCESS_STATES = [WEPAY_CAPTURED, WEPAY_SETTLED]
    WEPAY_PENDING_STATES = [WEPAY_AUTHORIZED]

    WEPAY_CHOICES = (
        (WEPAY_NEW, "New"),
        (WEPAY_AUTHORIZED, "Authorized"),
        (WEPAY_RESERVED, "Reserved"),
        (WEPAY_CAPTURED, "Captured"),
        (WEPAY_SETTLED, "Settled"),
        (WEPAY_CANCELLED, "Cancelled"),
        (WEPAY_REFUNDED, "Refunded"),
        (WEPAY_CHARGED_BACK, "Charged Back"),
        (WEPAY_FAILED, "Failed"),
        (WEPAY_EXPIRED, "Expired")
    )

    WEPAY_REVERSE = {
        "new": WEPAY_NEW,
        "authorized": WEPAY_AUTHORIZED,
        "reserved": WEPAY_RESERVED,
        "captured": WEPAY_CAPTURED,
        "settled": WEPAY_SETTLED,
        "cancelled": WEPAY_CANCELLED,
        "refunded": WEPAY_REFUNDED,
        "charged back": WEPAY_CHARGED_BACK,
        "failed": WEPAY_FAILED,
        "expired": WEPAY_EXPIRED,
    }

    fundraiser = models.ForeignKey(Fundraiser, related_name="donations")
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    net_amount = models.DecimalField(max_digits=10, decimal_places=2)
    donation_time = models.DateTimeField()
    reward_claim_time = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=255)
    is_anonymous = models.BooleanField(default=False)
    donation_note = models.TextField(blank=True, null=True)
    wepay_state = models.IntegerField(blank=True, null=True, choices=WEPAY_CHOICES)
    wepay_transaction = models.IntegerField(blank=True, null=True)
    selected_rewards = models.ManyToManyField("partners.Partner")

    def __unicode__(self):
        return ("$%s donation to %s by %s" % (self.amount, self.fundraiser, self.name)).encode('utf-8')

    def update_wepay_state(self, wepay_state):
        state = self.WEPAY_REVERSE[wepay_state]
        self.wepay_state = state
        self.save()
        if self.wepay_state in self.WEPAY_SUCCESS_STATES and self.reward_claim_time is None:
            self.email_rewards(self.get_rewards())

    def get_rewards(self):
        from crowdvance.partners.models import DiscountCode

        partners = self.selected_rewards.all()
        codes = {}
        for partner in partners:
            try:
                codes[partner] = DiscountCode.objects.filter(partner=partner,
                                                             donation=self,
                                                             active=True)[0]
            except:
                codes[partner] = DiscountCode.objects.filter(partner=partner,
                                                             donation=None,
                                                             active=True)[0]
                codes[partner].set_used(self)
        if self.reward_claim_time is None:
            self.email_rewards(codes)
        return codes

    def email_rewards(self, codes):
        message = render_to_string('fundraisers/rewards_email.html', {'codes': codes, 'thank_you_message': self.fundraiser.thank_you_message, 'mediaurl': settings.MEDIA_URL})
        msg = EmailMessage(_('Your Crowdvance Donation Rewards'), message, 'rewards@crowdvance.com', [self.email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
        self.reward_claim_time = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
        self.save()

    @property
    def wepay_success(self):
        return self.wepay_state in self.WEPAY_SUCCESS_STATES

    @property
    def rewards_available(self):
        if not self.reward_claim_time:
            return True
        now = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
        return now - self.reward_claim_time < datetime.timedelta(hours=1)

    def save(self, *args, **kwargs):
        wepay_charge = Decimal(settings.WEPAY_MINIMUM)\
                       if self.amount * Decimal(settings.WEPAY_CHARGE) < Decimal(settings.WEPAY_MINIMUM)\
                       else self.amount * Decimal(settings.WEPAY_CHARGE)
        self.net_amount = self.amount - (self.amount * Decimal(settings.CROWDVANCE_COMMISSION) + wepay_charge)
        if not self.donation_time:
            self.donation_time = datetime.datetime.today()
        super(Donation, self).save(*args, **kwargs)
